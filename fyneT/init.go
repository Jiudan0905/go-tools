package fyneT

import (
	"os"
	"strings"
	
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/driver/desktop"
	"github.com/flopp/go-findfont"
)

var isShow = true
var isTray = true

func initFont(font ...string) {
	defaultFont := "simkai.ttf"
	fontPaths := findfont.List()
	if len(font) > 0 {
		defaultFont = font[0]
	}
	for _, path := range fontPaths {
		if strings.Contains(path, defaultFont) {
			_ = os.Setenv("FYNE_FONT", path)
			break
		}
	}
}

func InitApp(title string, resources ...any) (fyne.App, fyne.Window) {
	// 初始化字体
	if len(resources) == 2 {
		initFont(resources[1].(string))
	} else {
		initFont()
	}
	// 初始化图标
	defaultIcon := ResourceIconPng
	myApp := app.New()
	if len(resources) >= 1 {
		defaultIcon = resources[0].(*fyne.StaticResource)
	}
	myApp.SetIcon(defaultIcon)
	// 启动
	myWin := myApp.NewWindow(title)
	myWin.CenterOnScreen()
	InitTray(myApp, myWin, title, defaultIcon)
	return myApp, myWin
}

func InitTray(app fyne.App, win fyne.Window, title string, icon fyne.Resource) {
	// 设置托盘
	if desk_, ok := app.(desktop.App); ok {
		show := fyne.NewMenuItem("显示&&隐藏", func() {
			if isShow {
				win.Hide()
				isShow = false
			} else {
				win.Show()
				isShow = true
			}
		})
		m := fyne.NewMenu(title, show)
		desk_.SetSystemTrayMenu(m)
		desk_.SetSystemTrayIcon(icon)
	}
	// 设置退出逻辑
	win.SetCloseIntercept(func() {
		if isTray {
			cnf := dialog.NewConfirm("提示", "需要将程序放置于托盘区而不是直接退出吗?", func(IsHide bool) {
				if IsHide {
					isTray = false
					isShow = false
					win.Hide()
				} else {
					app.Quit()
				}
			}, win)
			cnf.SetDismissText("退出")
			cnf.SetConfirmText("是的,我需要")
			cnf.Show()
		} else {
			isShow = false
			win.Hide()
		}
	})
}

func Show(win fyne.Window, width, height float32) {
	defer func() {
		err := recover()
		if err != nil {
			dialog.NewInformation("ERROR", err.(error).Error(), win)
		}
	}()
	win.Resize(fyne.Size{
		Width:  width,
		Height: height,
	})
	win.ShowAndRun()
}
