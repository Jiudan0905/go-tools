package fyneT

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/storage"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

func ShowFileOpenDialog(win fyne.Window, fileType_ string, f func(f fyne.URIReadCloser)) (fd *dialog.FileDialog) {
	fd = dialog.NewFileOpen(func(reader fyne.URIReadCloser, err error) {
		if err != nil {
			logT.Error(err.Error())
			dialog.ShowError(err, win)
			return
		}
		f(reader)
	}, win)
	fd.SetFilter(storage.NewExtensionFileFilter([]string{fileType_, "*"}))
	return fd
}

func ShowFileOpenDialogWithPath(win fyne.Window, path string, fileType_ string, f func(f fyne.URIReadCloser)) (fd *dialog.FileDialog) {
	fd = dialog.NewFileOpen(func(reader fyne.URIReadCloser, err error) {
		if err != nil {
			logT.Error(err.Error())
			dialog.ShowError(err, win)
			return
		}
		f(reader)
	}, win)
	ls, _ := storage.ListerForURI(storage.NewFileURI(path))
	fd.SetLocation(ls)
	fd.SetFilter(storage.NewExtensionFileFilter([]string{fileType_, "*"}))
	return fd
}
