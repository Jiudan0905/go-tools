package fyneT

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

func NewBtn(title string, importance ...widget.Importance) *widget.Button {
	btn := widget.NewButton(title, nil)
	if len(importance) > 0 {
		btn.Importance = importance[0]
	}
	return btn
}

func NewEntry(placeHolder string, disable ...bool) *widget.Entry {
	entry := widget.NewEntry()
	entry.SetPlaceHolder(placeHolder)
	if len(disable) > 0 && disable[0] {
		entry.Disable()
	}
	return entry
}

func NewSelect(placeHolder string, opts []string) *widget.Select {
	s := widget.NewSelect(opts, nil)
	s.PlaceHolder = placeHolder
	return s
}

func NewSelectEntry(filedList []string) *widget.SelectEntry {
	entry := widget.NewSelectEntry(filedList)
	entry.OnChanged = func(s string) {
		entry.Resize(fyne.Size{Width: 200, Height: 50})
	}
	return entry
}

func NewMultiLineEntry(text string) *widget.Entry {
	entry := widget.NewMultiLineEntry()
	entry.SetText(text)
	return entry
}
