package pdfT

import (
	"github.com/signintech/gopdf"
	
	"gitee.com/ydxj/gotls/baseT/logT"
	"gitee.com/ydxj/gotls/baseT/osT"
)

type DrawConfig struct {
	X         float64
	Y         float64
	Font      string
	FontSize  float64
	LineSpace float64
}

type PDF struct {
	File   *gopdf.GoPdf
	Width  float64
	Height float64
}

func MMToPixel(mm float64) float64 {
	return mm / 25.4 * 72
}

func GetPDF(mmWidth, mmHeight float64, fontPath string) *PDF {
	pdf := &gopdf.GoPdf{}
	// 设置页面大小
	pdf.Start(gopdf.Config{PageSize: gopdf.Rect{
		W: MMToPixel(mmWidth),
		H: MMToPixel(mmHeight),
	}})
	// 加载并设置字体
	_ = pdf.AddTTFFont("default_font", fontPath)
	_ = pdf.SetFont("default_font", "", 8)
	return &PDF{
		File:   pdf,
		Width:  MMToPixel(mmWidth),
		Height: MMToPixel(mmHeight),
	}
}

func (pdf *PDF) WriterText(config *DrawConfig, str []string) {
	if config.Font != "" {
		_ = pdf.File.SetFont(config.Font, "", config.FontSize)
	}
	err := pdf.File.SetFontSize(config.FontSize)
	if err != nil {
		logT.Error(err.Error())
	}
	for _, s := range str {
		pdf.File.SetXY(config.X, config.Y)
		err = pdf.File.Text(s)
		if err != nil {
			logT.Error(err.Error())
		}
		config.Y += config.LineSpace
	}
}

func (pdf *PDF) WriterImage(config *DrawConfig, imagePath string) {
	err := pdf.File.Image(imagePath, config.X, config.Y, nil) // print image
	if err != nil {
		logT.Error(err.Error())
	}
}

func (pdf *PDF) AddPage() {
	pdf.File.AddPage()
}

func (pdf *PDF) AddFont(fontPath string) {
	err := pdf.File.AddTTFFont(osT.GetFileName(fontPath), fontPath)
	if err != nil {
		logT.Error(err.Error())
	}
}

func (pdf *PDF) SaveAs(filepath string) {
	err := pdf.File.WritePdf(filepath)
	if err != nil {
		logT.Error(err.Error())
	}
}
