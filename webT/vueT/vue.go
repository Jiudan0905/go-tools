package vueT

import (
	"net/http"
	
	"github.com/gin-gonic/gin"
)

func Init(router *gin.Engine, urlList []string) {
	router.Static("/css", "./dist/css")
	router.Static("/img", "./dist/img")
	router.Static("/js", "./dist/js")
	router.LoadHTMLGlob("./dist/*.html")
	for _, url := range urlList {
		router.GET(url, func(c *gin.Context) {
			c.HTML(http.StatusOK, "index.html", gin.H{})
		})
	}
}
