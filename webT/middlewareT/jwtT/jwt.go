package jwtT

import (
	"errors"
	"time"
	
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

const (
	// jwt key
	Jwt         = "jwt"
	JwtMsg      = "jwt-msg"
	JwtAuth     = "jwt-auth"
	JwtUser     = "jwt-user"
	JwtUsername = "jwt-username"
	// jwt 错误信息
	JwtTokenError       = "jwt is error"
	JwtTokenEffective   = "jwt is effective"
	JwtTokenCantBeEmpty = "jwt cannot be empty"
	JwtTokenExpired     = "jwt is expired"
	JwtTokenNotValidYet = "jwt is not valid yet"
)

type JwtConfig struct {
	JwtExpiresTime int
	JwtSigningKey  string
}

type UserClaims struct {
	Name   string
	UserID int
	jwt.StandardClaims
}

var DefaultJwtConfig = &JwtConfig{
	JwtExpiresTime: 60 * 60 * 24 * 7,
	JwtSigningKey:  "tinytok",
}

func CreateToken(claims *UserClaims) (string, error) {
	claims.StandardClaims = jwt.StandardClaims{
		// 签名的生效时间
		NotBefore: time.Now().Unix(),
		// 过期时间
		ExpiresAt: time.Now().Unix() + int64(DefaultJwtConfig.JwtExpiresTime),
		Issuer:    "tinytok",
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(DefaultJwtConfig.JwtSigningKey)
}

func ParseToken(tokenString string) (*UserClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &UserClaims{}, func(token *jwt.Token) (i interface{}, e error) {
		return DefaultJwtConfig.JwtSigningKey, nil
	})
	if err != nil {
		var ve *jwt.ValidationError
		if errors.As(err, &ve) {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return nil, errors.New(JwtTokenError)
			} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
				return nil, errors.New(JwtTokenExpired)
			} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 {
				return nil, errors.New(JwtTokenNotValidYet)
			} else {
				return nil, errors.New(JwtTokenError)
			}
		}
	}
	if token != nil {
		if claims, ok := token.Claims.(*UserClaims); ok && token.Valid {
			return claims, nil
		}
		return nil, errors.New(JwtTokenError)
	} else {
		return nil, errors.New(JwtTokenError)
	}
}

func RefreshToken(tokenString string) (string, error) {
	jwt.TimeFunc = func() time.Time {
		return time.Unix(0, 0)
	}
	token, err := jwt.ParseWithClaims(tokenString, &UserClaims{}, func(token *jwt.Token) (interface{}, error) {
		return DefaultJwtConfig.JwtSigningKey, nil
	})
	if err != nil {
		return "", err
	}
	if claims, ok := token.Claims.(*UserClaims); ok && token.Valid {
		jwt.TimeFunc = time.Now
		claims.StandardClaims.ExpiresAt = time.Now().Add(1 * time.Hour).Unix()
		return CreateToken(claims)
	}
	return "", errors.New(JwtTokenError)
}

func JwtAuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// 从请求头中拿到token
		token := ctx.DefaultQuery(Jwt, "")
		// 部分接口使用form表单提交token
		if t, ok := ctx.GetPostForm(Jwt); ok {
			token = t
		}
		if token == "" {
			// 如果没有token
			ctx.Set(JwtAuth, false)
			ctx.Set(JwtMsg, JwtTokenCantBeEmpty)
			ctx.Next()
			return
		}
		// 解析jwt
		user, err := ParseToken(token)
		if err != nil {
			ctx.Set(JwtAuth, false)
			ctx.Set(JwtMsg, err.Error())
			ctx.Next()
			return
		}
		// token有效
		ctx.Set(JwtAuth, true)
		ctx.Set(JwtMsg, JwtTokenEffective)
		ctx.Set(JwtUser, user)
		ctx.Set(JwtUsername, user.Name)
		// 下一步
		ctx.Next()
		return
	}
}
