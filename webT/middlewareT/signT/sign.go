package signT

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"time"
	
	"github.com/gin-gonic/gin"
	
	"gitee.com/ydxj/gotls/baseT/randomT"
	"gitee.com/ydxj/gotls/baseT/strT"
)

var (
	Sign          = "sign"
	SignKey       = "sign-key"
	SignMsg       = "sign-msg"
	SignAuth      = "sign-auth"
	SignError     = "signature verification failed"
	SignEffective = "signature verification success"
	// 务必配置密钥，否则将无法通过签名验证
	SignSecret = randomT.GenRandomStr(32)
	// 签名过期时间，单位为毫秒
	SignTimeOut int64 = 60 * 10 * 1000
)

func GenerateSignature(data string, timestamp int64) string {
	combinedData := fmt.Sprintf("%d-%s-%s", timestamp, data, SignSecret)
	hash := sha256.Sum256([]byte(combinedData))
	return hex.EncodeToString(hash[:])
}

func VerifySignature(data string, signature string, timestamp int64) bool {
	// 检查时间是否在有效范围内
	currentTime := time.Now().UnixMilli()
	if currentTime-timestamp > SignTimeOut {
		return false
	}
	// 重新生成签名进行对比
	generatedSignature := GenerateSignature(data, timestamp)
	return generatedSignature == signature
}

func SignAuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// filename
		sign := ctx.DefaultPostForm(Sign, "")
		signKey := ctx.DefaultPostForm(SignKey, "")
		timestamp := strT.Atoi64(ctx.DefaultPostForm("timestamp", "-1"))
		// 验证签名
		if sign == "" || !VerifySignature(signKey, sign, timestamp) {
			ctx.Set(SignMsg, SignError)
			ctx.Set(SignAuth, false)
			ctx.Next()
			return
		}
		ctx.Set(SignAuth, true)
		ctx.Set(SignMsg, SignEffective)
		ctx.Next()
		return
	}
}
