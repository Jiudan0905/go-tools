package captchaT

import (
	"encoding/base64"
	"image/color"
	"net/http"
	"os"
	"strings"
	
	"github.com/gin-gonic/gin"
	"github.com/mojocn/base64Captcha"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

var store = base64Captcha.DefaultMemStore
var DefaultConfig = base64Captcha.DriverString{
	Height:          40,
	Width:           100,
	NoiseCount:      2,
	ShowLineOptions: 2 | 4,
	Length:          4,
	Source:          "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
	BgColor:         &color.RGBA{255, 255, 255, 255},
	Fonts:           []string{"wqy-microhei.ttc"},
}

func GenerateCaptcha() (id, b64s, answer string) {
	var driver base64Captcha.Driver
	driver = DefaultConfig.ConvertFonts()
	captcha := base64Captcha.NewCaptcha(driver, store)
	id, b64s, answer, err := captcha.Generate()
	if err != nil {
		logT.Error(err.Error())
	}
	return
}

func VerifyCaptcha(id, val string) bool {
	return store.Verify(id, val, true)
}

func SaveCaptcha(imageBase64, path string) {
	// 去除前缀
	prefix := "data:image/png;base64,"
	if strings.HasPrefix(imageBase64, prefix) {
		imageBase64 = imageBase64[len(prefix):]
	}
	imgBytes, err := base64.StdEncoding.DecodeString(imageBase64)
	if err != nil {
		logT.Error(err.Error())
		return
	}
	// 创建图片文件
	file, err := os.Create(path)
	if err != nil {
		logT.Error(err.Error())
		return
	}
	defer file.Close()
	_, err = file.Write(imgBytes)
	if err != nil {
		logT.Error(err.Error())
		return
	}
}

func InitCaptcha(router *gin.Engine) {
	// http://127.0.0.1:10039/api/captcha
	router.POST("/api/captcha", func(ctx *gin.Context) {
		id, data, _ := GenerateCaptcha()
		ctx.JSON(http.StatusOK, gin.H{
			"id":   id,
			"data": data,
		})
	})
	// http://127.0.0.1:10039/api/verify-captcha/:id/:captcha
	router.POST("/api/verify-captcha/:id/:captcha", func(ctx *gin.Context) {
		id := ctx.Param("id")
		captcha := ctx.Param("captcha")
		ctx.JSON(http.StatusOK, gin.H{
			"result": VerifyCaptcha(id, captcha),
		})
	})
}
