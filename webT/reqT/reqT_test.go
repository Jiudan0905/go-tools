package reqT

import (
	"fmt"
	"testing"
)

func TestGet(t *testing.T) {
	rst := map[string]any{}
	Get("http://127.0.0.1:10023/ping", &rst, map[string]string{
		"a": "1",
		"b": "2",
	})
	
	fmt.Println(rst)
}
