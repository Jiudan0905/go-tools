package reqT

import (
	"fmt"
	"time"
	
	"github.com/levigross/grequests"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

var DefaultTimeout = 3 * time.Second
var DefaultUserAgent = `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.6045.160 Safari/537.36`
var DefaultHeaders = map[string]string{
	"DefaultToken": "Author--YouDianXinJi--v20240517",
}

// 发送GET请求
func Get(url string, rst any, params map[string]string) {
	rsp, err := grequests.Get(url, &grequests.RequestOptions{
		Params:         params,
		UserAgent:      DefaultUserAgent,
		Headers:        DefaultHeaders,
		RequestTimeout: DefaultTimeout,
	})
	if rsp == nil {
		logT.Log(fmt.Sprintf("Get（%s）请求失败；服务器未返回任何响应", url))
		return
	}
	if err != nil {
		logT.Log(err.Error())
		return
	}
	if err = rsp.JSON(&rst); err != nil {
		return
	}
	return
}

// 获取字符串
func GetRspString(url string, params map[string]string) string {
	rsp, err := grequests.Get(url, &grequests.RequestOptions{
		Params:         params,
		UserAgent:      DefaultUserAgent,
		Headers:        DefaultHeaders,
		RequestTimeout: DefaultTimeout,
	})
	if rsp == nil {
		logT.Log(fmt.Sprintf("Get（%s）请求失败；服务器未返回任何响应", url))
		return ""
	}
	if err != nil {
		logT.Log(err.Error())
		return ""
	}
	return rsp.String()
}

// 发送GET请求
func Post(url string, rst any, data map[string]string) {
	rsp, err := grequests.Post(url, &grequests.RequestOptions{
		Data:           data,
		UserAgent:      DefaultUserAgent,
		Headers:        DefaultHeaders,
		RequestTimeout: DefaultTimeout,
	})
	if rsp == nil {
		logT.Log(fmt.Sprintf("Post（%s）请求失败；服务器未返回任何响应", url))
		return
	}
	if err != nil {
		logT.Log(err.Error())
		return
	}
	if err = rsp.JSON(&rst); err != nil {
		return
	}
	return
}
