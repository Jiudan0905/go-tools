package excelT

import (
	"github.com/xuri/excelize/v2"
)

// 颜色填充
func (cell *Cell) SetFillWarnStyle() {
	cell.GetStyle().Fill = excelize.Fill{Type: "pattern", Color: []string{"FFFF00"}, Pattern: 1}
	cell.SetStyle(cell.GetStyle())
}

func (cell *Cell) SetFillErrorStyle() {
	cell.GetStyle().Fill = excelize.Fill{Type: "pattern", Color: []string{"FF0000"}, Pattern: 1}
	cell.SetStyle(cell.GetStyle())
}

func (cell *Cell) SetFillStyle(color string) {
	cell.GetStyle().Fill = excelize.Fill{Type: "pattern", Color: []string{color}, Pattern: 1}
	cell.SetStyle(cell.GetStyle())
}

// 字体
func (cell *Cell) SetFontWarnStyle() {
	cell.GetStyle().Font = &excelize.Font{
		Color: "FFFF00",
	}
	cell.SetStyle(cell.GetStyle())
}

func (cell *Cell) SetFontErrorStyle() {
	cell.GetStyle().Font = &excelize.Font{
		Color: "FF0000",
	}
	cell.SetStyle(cell.GetStyle())
}

func (cell *Cell) SetFontStyle(color string) {
	cell.GetStyle().Font = &excelize.Font{
		Color: color,
	}
	cell.SetStyle(cell.GetStyle())
}

// 缩进
func (cell *Cell) GetIndent() int {
	return cell.GetStyle().Alignment.Indent
}

func (cell *Cell) SetIndent(indent int) {
	cell.GetStyle().Alignment.Indent = indent
	cell.SetStyle(cell.GetStyle())
}

// 批注
func (cell *Cell) SetComment(title, comment string, lengthWidthHeight ...uint) {
	c := excelize.Comment{
		Author: "YouDianXinJi",
		Cell:   cell.Position,
		Width:  250,
		Paragraph: []excelize.RichTextRun{
			{Text: title, Font: &excelize.Font{Color: "FF0000", Bold: true}},
			{Text: comment, Font: &excelize.Font{Bold: true}},
		},
	}
	if len(lengthWidthHeight) == 0 {
		_ = cell.File.AddComment(cell.SheetName, c)
	} else if len(lengthWidthHeight) == 1 {
		c.Width = lengthWidthHeight[0]
		_ = cell.File.AddComment(cell.SheetName, c)
	} else {
		c.Width = lengthWidthHeight[0]
		c.Height = lengthWidthHeight[1]
		_ = cell.File.AddComment(cell.SheetName, c)
	}
}

// 冻结窗格
func (s *Sheet) GetPanes() *excelize.Panes {
	panes, err := s.File.GetPanes(s.Name)
	if err != nil {
		panic("获取冻结窗格失败: " + err.Error())
	}
	return &panes
}

func (s *Sheet) SetPanes(panes *excelize.Panes) {
	err := s.File.SetPanes(s.Name, panes)
	if err != nil {
		panic("获取冻结窗格失败: " + err.Error())
	}
}
