package excelT

import (
	"fmt"
	
	"github.com/xuri/excelize/v2"
)

// 获取单元格合并单元格
func (s *Sheet) GetMergeCells() []excelize.MergeCell {
	cells, _ := s.File.GetMergeCells(s.Name)
	s.MergeCellList = cells
	return cells
}

// 合并单元格
func (s *Sheet) SetMergeCells(startAxis, endAxis string) {
	err := s.File.MergeCell(s.Name, startAxis, endAxis)
	if err != nil {
		panic(fmt.Sprintf("Target: %s [%s:%s] -> 合并单元格失败: %s", s.Name, startAxis, endAxis, err))
	}
}
