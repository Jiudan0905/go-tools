package excelT

// 创建工作表
func (e *Excel) NewSheet(sheetName string) {
	_, _ = e.File.NewSheet(sheetName)
}

// 重写工作表
func (e *Excel) ReWriteSheet(sheetName string) {
	_ = e.File.DeleteSheet(sheetName)
	_, _ = e.File.NewSheet(sheetName)
}

// 删除工作表
func (e *Excel) RemoveSheet(sheetName string) {
	_ = e.File.DeleteSheet(sheetName)
}
