package excelT

import (
	"fmt"
	
	"github.com/xuri/excelize/v2"
)

// 当单元格本身没有值时，设置单元格的值
func (s *Sheet) AddCellValByPos(position string, val any) {
	_ = s.File.SetCellValue(s.Name, position, val)
}

func (s *Sheet) AddCellStrValByPos(position string, val string) {
	_ = s.File.SetCellStr(s.Name, position, val)
}

func (s *Sheet) AddCellIntValByPos(position string, val int) {
	_ = s.File.SetCellInt(s.Name, position, val)
}

func (s *Sheet) AddCellFloatValByPos(position string, val float64, precision, bitSize int) {
	_ = s.File.SetCellFloat(s.Name, position, val, precision, bitSize)
}

func (s *Sheet) AddCellBoolValByPos(position string, val bool) {
	_ = s.File.SetCellBool(s.Name, position, val)
}

func (s *Sheet) AddCellValueAndStyleByPos(position string, val any, style *excelize.Style) {
	err := s.File.SetCellValue(s.Name, position, val)
	if err != nil {
		panic(fmt.Sprintf("设置Excel单元格值失败: %s", err))
	}
	cell := Cell{
		File:      s.File,
		SheetName: s.Name,
		Position:  position,
	}
	cell.SetStyle(style)
}

func (s *Sheet) AddCellVal(rowIndex, colIndex int, val any) {
	_ = s.File.SetCellValue(s.Name, GetCellPosition(rowIndex, colIndex), val)
}

func (s *Sheet) AddCellStrVal(rowIndex, colIndex int, val string) {
	_ = s.File.SetCellStr(s.Name, GetCellPosition(rowIndex, colIndex), val)
}

func (s *Sheet) AddCellIntVal(rowIndex, colIndex int, val int) {
	_ = s.File.SetCellInt(s.Name, GetCellPosition(rowIndex, colIndex), val)
}

func (s *Sheet) AddCellFloatVal(rowIndex, colIndex int, val float64, precision, bitSize int) {
	_ = s.File.SetCellFloat(s.Name, GetCellPosition(rowIndex, colIndex), val, precision, bitSize)
}

func (s *Sheet) AddCellBoolVal(rowIndex, colIndex int, val bool) {
	_ = s.File.SetCellBool(s.Name, GetCellPosition(rowIndex, colIndex), val)
}

func (s *Sheet) AddCellValueAndStyle(rowIndex, colIndex int, val any, style *excelize.Style) {
	err := s.File.SetCellValue(s.Name, GetCellPosition(rowIndex, colIndex), val)
	if err != nil {
		panic(fmt.Sprintf("设置Excel单元格值失败: %s", err))
	}
	cell := Cell{
		File:      s.File,
		SheetName: s.Name,
		Position:  GetCellPosition(rowIndex, colIndex),
	}
	cell.SetStyle(style)
}

// 当单元格本身有值时，设置单元格的值
func (cell *Cell) SetCellValue(val any) {
	_ = cell.File.SetCellValue(cell.SheetName, GetCellPosition(cell.RowIndex, cell.ColIndex), val)
}

func (cell *Cell) SetCellStrValue(val string) {
	_ = cell.File.SetCellStr(cell.SheetName, GetCellPosition(cell.RowIndex, cell.ColIndex), val)
}

func (cell *Cell) SetCellIntValue(val int) {
	_ = cell.File.SetCellInt(cell.SheetName, GetCellPosition(cell.RowIndex, cell.ColIndex), val)
}

func (cell *Cell) SetCellFloatValue(val float64) {
	_ = cell.File.SetCellFloat(cell.SheetName, GetCellPosition(cell.RowIndex, cell.ColIndex), val, 2, 64)
}

func (cell *Cell) SetCellBoolValue(val bool) {
	_ = cell.File.SetCellBool(cell.SheetName, GetCellPosition(cell.RowIndex, cell.ColIndex), val)
}

func (cell *Cell) SetCellValueAndStyle(val any, style *excelize.Style) {
	err := cell.File.SetCellValue(cell.SheetName, cell.Position, val)
	if err != nil {
		panic(fmt.Sprintf("设置Excel单元格值失败: %s", err))
	}
	cell.SetStyle(style)
}
