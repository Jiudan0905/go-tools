package excelT

import (
	"strings"
)

func (r *Row) InsertCols(col string, num int) {
	_ = r.File.InsertCols(r.SheetName, strings.ToUpper(col), num)
}

func (r *Row) InsertCol(col string) {
	_ = r.File.InsertCols(r.SheetName, strings.ToUpper(col), 1)
}
