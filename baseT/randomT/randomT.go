package randomT

import (
	"time"
	
	"github.com/awnumar/fastrand"
)

// 生成随机字符串
func GenRandomStr(length int, config ...bool) string {
	charset := ""
	charsetList := []string{"abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789", "!@#$%^&*()"}
	for i := range charsetList {
		if i < len(config) && config[i] {
			charset += charsetList[i]
		}
	}
	result := make([]byte, length)
	
	if len(charset) == 0 {
		return ""
	}
	
	for i := 0; i < length; i++ {
		randomIndex := fastrand.Intn(len(charset))
		result[i] = charset[randomIndex]
	}
	
	return string(result)
}

// 生成随机数字
func GenRandomNum(num1, num2 int) int {
	return fastrand.Intn(num2-num1) + num1
}

// 生成随机日期
func GenRandomDate(start, end time.Time) time.Time {
	return start.Add(time.Duration(fastrand.Intn(int(end.Sub(start).Seconds()))) * time.Second)
}
