package randomT

import (
	"fmt"
	"testing"
	"time"
)

func TestRandom(t *testing.T) {
	fmt.Println(GenRandomNum(1, 10))
	fmt.Println(GenRandomStr(15))
	fmt.Println(GenRandomDate(time.Now(), time.Now().Add(time.Hour*24)))
}
