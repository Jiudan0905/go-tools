package socketT

import (
	"fmt"
	"testing"
	"time"
)

func TestServer(t *testing.T) {
	ln := NewServer(8080)
	go ReceiveMsg(ln, func(msg string) string {
		return "Server Receive: " + msg
	})
	time.Sleep(time.Second * 3)
	client1 := NewClient("127.0.0.1", 8080)
	client2 := NewClient("127.0.0.1", 8080)
	client3 := NewClient("127.0.0.1", 8080)
	fmt.Println(SendMsg(client1, "hello1"))
	fmt.Println(SendMsg(client2, "hello2"))
	fmt.Println(SendMsg(client3, "hello3"))
	time.Sleep(time.Second * 10)
}
