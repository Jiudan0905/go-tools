package numT

import (
	"github.com/shopspring/decimal"
)

func MinInt(num int, nums ...int) int {
	for _, n := range nums {
		if n < num {
			num = n
		}
	}
	return num
}

func MaxInt(num int, nums ...int) int {
	for _, n := range nums {
		if n > num {
			num = n
		}
	}
	return num
}

func Divide(a int, b int, decimalPlaces ...int) float64 {
	decimalA := decimal.NewFromInt(int64(a))
	decimalB := decimal.NewFromInt(int64(b))
	decimalResult := decimalA.Div(decimalB)
	if len(decimalPlaces) == 1 {
		roundedResult, _ := decimalResult.Round(int32(decimalPlaces[0])).Float64()
		return roundedResult
	}
	return decimalResult.InexactFloat64()
}
