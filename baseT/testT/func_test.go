package testT

import (
	"fmt"
	"testing"
	
	"gitee.com/ydxj/gotls/baseT/encryptT/totpT"
	"gitee.com/ydxj/gotls/baseT/strT"
)

func TestContainsOnlyEnglishAlNumUnderscores(t *testing.T) {
	str1 := "a123456|"
	fmt.Println(strT.ContainsOnlyEnglishAlNumUnderscores(str1))
	str2 := "Author YouDianXinJi"
	fmt.Println(strT.ContainsOnlyEnglishAlNumUnderscores(str2, "a-z1-9A-Z_ "))
	str3 := "中文abc"
	fmt.Println(strT.ContainsOnlyEnglishAlNumUnderscores(str3, "中文a-z"))
}

func TestGetTotpToken(t *testing.T) {
	token := totpT.GetTotpToken("Jiudan")
	fmt.Print(token)
}
