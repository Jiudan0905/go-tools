package clipboardT

import (
	"github.com/atotto/clipboard"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

func WriteClipboard(text string) error {
	err := clipboard.WriteAll(text)
	if err != nil {
		logT.Error("写入粘贴板失败: " + err.Error())
		return err
	}
	return nil
}

func ReadClipboard() string {
	text, err := clipboard.ReadAll()
	if err != nil {
		logT.Error("读取粘贴板失败: " + err.Error())
	}
	return text
}
