package configT

import (
	"fmt"
	"testing"
	
	"gitee.com/ydxj/gotls/baseT/testT"
)

func TestGetConfig(t *testing.T) {
	testT.InitTestEnv()
	config := NewYamlConfig("./baseT/configT/test.yaml")
	// config := NewConfig("./baseT/configT/test.yaml")
	fmt.Println(config.ReadInt("Test.Num"))
	fmt.Println(config.ReadString("Test.Str"))
	fmt.Println(config.ReadString("Test.NotOk", "hello"))
	// 测试加密
	GenEncryptConfig("./baseT/configT/test.yaml", "123456")
	encryptConfig := NewEncryptConfig("./baseT/configT/test.yaml.age", "123456")
	fmt.Println(encryptConfig.ReadString("Test.Num"))
}
