package configT

import (
	"gitee.com/ydxj/gotls/baseT/encryptT/ageT"
	"gitee.com/ydxj/gotls/baseT/fileT"
	"gitee.com/ydxj/gotls/baseT/logT"
)

func encrypt(configPath, encryptConfigPath, key string) {
	// 读取原配置文件
	content := fileT.ReadFile(configPath)
	// 使用指定的key对配置文件进行加密
	err := ageT.EncryptToFile([]byte(content), encryptConfigPath, key)
	if err != nil {
		logT.Error("Encrypt Error: " + err.Error())
		return
	}
}

func decrypt(encryptConfigPath, configPath, key string) {
	// 使用指定的key对配置文件进行解密
	data, err := ageT.DecryptFromFile(encryptConfigPath, key)
	if err != nil {
		logT.Error("Decrypt Error: " + err.Error())
		return
	}
	// 将解密后的内容写入配置文件
	fileT.WriteFile(configPath, string(data))
}
