package ageT

import (
	"bytes"
	"io"
	"os"
	"sync"
	"time"
	
	"filippo.io/age"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

var cacheMutex sync.Mutex
var DecryptCache sync.Map

// 清除缓存
func ClearCache() {
	cacheMutex.Lock()
	defer cacheMutex.Unlock()
	// 清除缓存
	DecryptCache = sync.Map{}
}

// 自动清除缓存
func AutoClearCache(d time.Duration) {
	ticker := time.NewTicker(d)
	go func() {
		for range ticker.C {
			ClearCache()
		}
	}()
}

// 加密
func Encrypt(data []byte, key string) ([]byte, error) {
	// 创建一个新的加密/解密密钥对
	recipient, err := age.NewScryptRecipient(key)
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	
	// 加密数据
	var cipherText bytes.Buffer
	writer, err := age.Encrypt(&cipherText, recipient)
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	
	_, err = writer.Write(data)
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	
	_ = writer.Close()
	return cipherText.Bytes(), nil
}

// 解密
func Decrypt(cipherData []byte, key string) ([]byte, error) {
	// 创建一个新的解密接收者
	identity, err := age.NewScryptIdentity(key)
	if err != nil {
		logT.Error(err.Error())
		return []byte(""), err
	}
	
	// 解密数据
	reader, err := age.Decrypt(bytes.NewReader(cipherData), identity)
	if err != nil {
		logT.Error(err.Error())
		return []byte(""), err
	}
	
	var plainText bytes.Buffer
	_, err = io.Copy(&plainText, reader)
	if err != nil {
		logT.Error(err.Error())
		return []byte(""), err
	}
	
	return plainText.Bytes(), nil
}

// 加密并写入缓存
func EncriptWithCache(plainText string, key string, psdKey string) ([]byte, error) {
	DecryptCache.Delete(psdKey)
	psd, err := Encrypt([]byte(plainText), key)
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	DecryptCache.Store(psdKey, plainText)
	return psd, err
}

// 解密并写入缓存
func DecryptWithCache(cipherText []byte, key string, psdKey string) (string, error) {
	// 使用缓存
	result, ok := DecryptCache.Load(psdKey)
	if ok {
		return result.(string), nil
	}
	// 将密钥写入缓存中
	dp, err := Decrypt(cipherText, key)
	if err != nil {
		logT.Error(err.Error())
		return "", err
	}
	DecryptCache.Store(psdKey, string(dp))
	return string(dp), nil
}

// 加密的数据, 数据即将存储的位置, 加密密钥
func EncryptToFile(buf []byte, encryptedFilePath string, key string) (err error) {
	cipherData, err := Encrypt(buf, key)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	// 将数据写入文件
	err = os.WriteFile(encryptedFilePath, cipherData, 0644)
	if err != nil {
		logT.Error(err.Error())
		return nil
	}
	return nil
}

// 加密的文件, 加密密钥
func DecryptFromFile(decryptFilePath string, key string) (data []byte, err error) {
	buf, err := os.ReadFile(decryptFilePath)
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	data, err = Decrypt(buf, key)
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	return
}
