package encryptT

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

// 生成Rsa的公钥和私钥
func GenerateRSAKey(bits int) ([]byte, []byte) {
	// 私钥
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		logT.Error(err.Error())
		panic(err)
	}
	X509PrivateKey := x509.MarshalPKCS1PrivateKey(privateKey)
	privateBlock := pem.Block{Type: "RSA Private Key", Bytes: X509PrivateKey}
	// 公钥
	publicKey := privateKey.PublicKey
	X509PublicKey, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		logT.Error(err.Error())
		panic(err)
	}
	publicBlock := pem.Block{Type: "RSA Public Key", Bytes: X509PublicKey}
	// 输出
	return pem.EncodeToMemory(&publicBlock), pem.EncodeToMemory(&privateBlock)
}

func EncryptRSA(plainText string, buf []byte) []byte {
	block, _ := pem.Decode(buf)
	publicKeyInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		panic(err)
	}
	publicKey := publicKeyInterface.(*rsa.PublicKey)
	cipherText, err := rsa.EncryptPKCS1v15(rand.Reader, publicKey, []byte(plainText))
	if err != nil {
		logT.Error(err.Error())
		panic(err)
	}
	return cipherText
}

func DecryptRSA(cipherText []byte, buf []byte) string {
	block, _ := pem.Decode(buf)
	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		logT.Error(err.Error())
		panic(err)
	}
	plainText, _ := rsa.DecryptPKCS1v15(rand.Reader, privateKey, cipherText)
	return string(plainText)
}
