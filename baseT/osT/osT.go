package osT

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

// 判断是否是目录
func IsDir(name string) bool {
	if info, err := os.Stat(name); err == nil {
		return info.IsDir()
	}
	return false
}

// 判断是否是文件
func IsFile(name string) bool {
	if info, err := os.Stat(name); err == nil {
		return !info.IsDir()
	}
	return false
}

// 判断文件是否存在
func FileIsExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

// 获取文件目录
func GetFileDir(path string) string {
	return filepath.Dir(path)
}

// 获取文件绝对路径
func GetFileAbsDir(path string) string {
	path, _ = filepath.Abs(path)
	return filepath.Dir(path)
}

// 获取文件名
func GetFileName(path string) string {
	baseName := filepath.Base(path)
	// 去掉文件名的后缀
	baseName = baseName[:len(baseName)-len(filepath.Ext(baseName))]
	return baseName
}

// 获取文件后缀
func GetFileSuffix(path string) string {
	baseName := filepath.Base(path)
	return filepath.Ext(baseName)
}

// 获取文件名带后缀
func GetFileNameWithSuffix(path string) string {
	return filepath.Base(path)
}

// 获取目录下所有文件
func GetDirFile(root string, getDir bool, level int) []string {
	var rst []string
	if strings.HasSuffix(root, `\`) || strings.HasSuffix(root, `/`) {
		root = root[:len(root)-1]
	}
	if level != -1 {
		level = level + strings.Count(root, `/`) + strings.Count(root, `\`)
	}
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			logT.Error(err.Error())
			return err
		}
		// 如果是目录的情况下并且不允许采集目录则直接返回
		if !getDir && info.IsDir() {
			return nil
		}
		// 如果是一级目录的情况下并且不允许采集一级目录则直接返回
		if level != -1 && strings.Count(path, `/`)+strings.Count(path, `\`) > level {
			return nil
		}
		// 将其转换为绝对路径
		path, _ = filepath.Abs(path)
		// 其他情况则采集
		rst = append(rst, path)
		return nil
	})
	if err != nil {
		logT.Error(err.Error())
	}
	return rst
}

// 获取一级目录下的文件列表
func GetFileListLevel1(root string, getDir ...bool) []string {
	if len(getDir) > 0 && getDir[0] {
		return GetDirFile(root, true, 1)
	}
	return GetDirFile(root, false, 1)
}

// 文件复制
func CopyFile(src string, dst ...string) {
	srcFile, err := os.Open(src)
	if err != nil {
		logT.Error(err.Error())
		return
	}
	defer srcFile.Close()
	copyFile := GetFileDir(src) + "/" + GetFileName(src) + ".copy" + GetFileSuffix(src)
	if len(dst) > 0 && FileIsExists(dst[0]) {
		copyFile = dst[0]
	}
	dstFile, err := os.Create(copyFile)
	if err != nil {
		logT.Error(err.Error())
		return
	}
	defer dstFile.Close()
	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		logT.Error(err.Error())
		return
	}
	return
}

// 文件重命名
func RenameFile(old, new string) {
	// 替换为实际的旧文件名和新文件名
	err := os.Rename(old, new)
	if err != nil {
		logT.Error(err.Error())
	}
}

// 创建文件夹
func MakeDirs(dir string) {
	if !FileIsExists(dir) {
		if err := os.MkdirAll(dir, 0777); err != nil {
			logT.Error(err.Error())
		}
	}
}

// 获取cmd输入
func GetCmdInput(tips string, ignoreCase bool, endChar ...string) string {
	fmt.Print(tips)
	reader := bufio.NewReader(os.Stdin)
	var text string
	// 判断是否结束
	end := "\n"
	if len(endChar) > 0 {
		end = endChar[0]
	}
	for {
		input, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("读取输入出现错误: %v\n", err)
			panic("读取输入出现错误")
		}
		input = strings.TrimSpace(input)
		text += input
		// 如果忽略大小写
		if ignoreCase {
			input = strings.ToLower(input)
			end = strings.ToLower(end)
		}
		if strings.Contains(input, end) || end == "\n" {
			break
		}
		// 添加换行符
		text += "\n"
	}
	return text
}

func GetCmdInputString(tips string, endChar ...string) string {
	return GetCmdInput(tips, false, endChar...)
}

func GetCmdInputInt(tips string, endChar ...string) int {
	rst := GetCmdInput(tips, false, endChar...)
	// 将结果转为int
	intRst, err := strconv.Atoi(rst)
	if err != nil {
		logT.Error("输入错误")
	}
	return intRst
}

func GetCmdInputFloat(tips string, endChar ...string) float64 {
	rst := GetCmdInput(tips, false, endChar...)
	// 将结果转为float
	floatRst, err := strconv.ParseFloat(rst, 64)
	if err != nil {
		logT.Error("输入错误")
	}
	return floatRst
}

// 从命令行参数中获取参数
func GetCmdArgs(key, defaultValue, help string) *string {
	value := flag.String(key, defaultValue, help)
	return value
}

func GetCmdArgsInt(key string, defaultValue int, help string) *int {
	value := flag.Int(key, defaultValue, help)
	return value
}

func GetCmdArgsFloat(key string, defaultValue float64, help string) *float64 {
	value := flag.Float64(key, defaultValue, help)
	return value
}

func GetCmdArgsBool(key string, defaultValue bool, help string) *bool {
	value := flag.Bool(key, defaultValue, help)
	return value
}

func FlagParse() {
	flag.Parse()
}
