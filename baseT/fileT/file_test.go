package fileT

import (
	"fmt"
	"testing"
)

func TestFileT(t *testing.T) {
	CreatFile("test.txt", "-- create hello world - 1")
	fmt.Println(ReadFile("test.txt"))
	CreatFile("test.txt", "-- create hello world - 2")
	fmt.Println(ReadFile("test.txt"))
	WriteFile("test.txt", "-- write hello world")
	fmt.Println(ReadFile("test.txt"))
	AppendFile("test.txt", "-- append hello world")
	fmt.Println(ReadFile("test.txt"))
	WriteFile("test.txt", "-- write hello world")
	fmt.Println(ReadFile("test.txt"))
}
