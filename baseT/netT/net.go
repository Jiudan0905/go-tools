package netT

import (
	"net"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

// 获取一个空闲的端口
func GetFreePort() int {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		logT.Error(err.Error())
	}
	TCPListener, _ := net.ListenTCP("tcp", addr)
	defer TCPListener.Close()
	return TCPListener.Addr().(*net.TCPAddr).Port
}

// 获取本地IP地址
func GetLocalIP() string {
	ipAddr, err := net.ResolveIPAddr("ip", "www.baidu.com")
	if err != nil {
		logT.Error(err.Error())
		return ""
	}
	rAddr, err1 := net.ResolveIPAddr("ip4:icmp", ipAddr.String())
	lAddr, err2 := net.ResolveIPAddr("ip4:icmp", "")
	con, err3 := net.DialIP("ip4:icmp", lAddr, rAddr)
	if err1 != nil || err2 != nil || err3 != nil {
		return ""
	}
	defer con.Close()
	return con.LocalAddr().String()
}
