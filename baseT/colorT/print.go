package colorT

import "fmt"

// 前景	背景	 颜色
// 30	40	 黑色
// 31	41	 红色
// 32	42	 绿色
// 33	43	 黄色
// 34	44	 蓝色
// 35	45	 紫色
// 36	46	 深绿
// 37	47	 白色

type ForegroundColor int
type BackgroundColor int
type Mode int

type Color struct {
	ForegroundColor ForegroundColor
	BackgroundColor BackgroundColor
	Mode            Mode
}

var (
	// 前景色
	Black     = ForegroundColor(30)
	Red       = ForegroundColor(31)
	Green     = ForegroundColor(32)
	Yellow    = ForegroundColor(33)
	Blue      = ForegroundColor(34)
	Purple    = ForegroundColor(35)
	DarkGreen = ForegroundColor(36)
	White     = ForegroundColor(37)
	// 背景色
	BlackBGColor     = BackgroundColor(40)
	RedBGColor       = BackgroundColor(41)
	GreenBGColor     = BackgroundColor(42)
	YellowBGColor    = BackgroundColor(43)
	BlueBGColor      = BackgroundColor(44)
	PurpleBGColor    = BackgroundColor(45)
	DarkGreenBGColor = BackgroundColor(46)
	WhiteBGColor     = BackgroundColor(47)
	// 0：默认模式，无特殊效果
	DEFAULT = Mode(0)
	// 1：粗体模式
	BOLD = Mode(1)
	// 2：淡化模式
	FAINT = Mode(2)
	// 3：斜体模式
	ITALIC = Mode(3)
	// 4：下划线模式
	UNDERLINE = Mode(4)
	// 5：闪烁模式
	BLINK = Mode(5)
	// 7：反显模式
	REVERSE = Mode(6)
	// 8：隐藏模式
	HIDDEN = Mode(7)
)

func String(str string, color Color) string {
	if color.Mode == 0 {
		if color.BackgroundColor == 0 {
			return fmt.Sprintf("\x1b[%dm%s\x1b[0m", color.ForegroundColor, str)
		} else {
			return fmt.Sprintf("\x1b[%d;%dm%s\x1b[0m", color.BackgroundColor, color.ForegroundColor, str)
		}
	} else {
		if color.BackgroundColor == 0 {
			return fmt.Sprintf("\x1b[%d;%dm%s\x1b[0m", color.ForegroundColor, color.Mode, str)
		} else {
			return fmt.Sprintf("\x1b[%d;%d;%dm%s\x1b[0m", color.BackgroundColor, color.ForegroundColor, color.Mode, str)
		}
	}
}

func Println(str string, color Color) {
	fmt.Println(String(str, color))
}

func InfoString(str string) string {
	color := Color{
		ForegroundColor: Blue,
		BackgroundColor: BlackBGColor,
		Mode:            BOLD,
	}
	return String(str, color)
}

func WarnString(str string) string {
	color := Color{
		ForegroundColor: Yellow,
		BackgroundColor: BlackBGColor,
		Mode:            BOLD,
	}
	return String(str, color)
}

func ErrorString(str string) string {
	color := Color{
		ForegroundColor: Red,
		BackgroundColor: BlackBGColor,
		Mode:            BOLD,
	}
	return String(str, color)
}

func PrintlnInfo(str string) {
	fmt.Println(InfoString(str))
}

func PrintlnWarn(str string) {
	fmt.Println(WarnString(str))
}

func PrintlnError(str string) {
	fmt.Println(ErrorString(str))
}
