package strT

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

// 判断字符串含有某些字符
func ContainsOnlyEnglishAlNumUnderscores(str string, reStr ...string) bool {
	pattern := ""
	if len(reStr) == 0 {
		pattern = `^[a-zA-Z0-9_~!@#$%^&*()_+,.;'/\|]+$`
	} else {
		pattern = fmt.Sprintf("^[%s]+$", reStr[0])
	}
	matched, err := regexp.MatchString(pattern, str)
	if err != nil {
		logT.Error(err.Error())
	}
	return matched
}

// 检查空字符串 - 有一个为空则返回 True
func CheckEmptyString(vals ...string) bool {
	for _, val := range vals {
		if val == "" {
			return true
		}
	}
	return false
}

// 将字符串转换为数字类型
func Atoi(str string) int {
	str = strings.ReplaceAll(str, ",", "")
	result, err := strconv.Atoi(str)
	if err != nil {
		return -1
	}
	return result
}

func Atoi32(str string) int32 {
	str = strings.ReplaceAll(str, ",", "")
	result, err := strconv.Atoi(str)
	if err != nil {
		return -1
	}
	return int32(result)
}

func Atoi64(str string) int64 {
	str = strings.ReplaceAll(str, ",", "")
	result, err := strconv.Atoi(str)
	if err != nil {
		return -1
	}
	return int64(result)
}

func AtoFloat(str string) float64 {
	str = strings.ReplaceAll(str, ",", "")
	if floatValue, err := strconv.ParseFloat(str, 64); err == nil {
		return floatValue
	}
	return -1
}
